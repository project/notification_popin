<?php

namespace Drupal\notification_popin;

use \Drupal\Core\Datetime\DrupalDateTime;
use \Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use \Drupal\node\Entity\Node;


class NotificationManager {

    public function getNotifications(string $path, $nid): array {
        $now = new DrupalDateTime();
        $now = $now->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
        $roles = \Drupal::currentUser()->getRoles();

        $query = \Drupal::entityQuery('notification')->accessCheck(FALSE);
        $query->condition('status', 1);

        $condition_start_date = $query->orConditionGroup();
        $condition_start_date->notExists('start_date');
        $condition_start_date->condition('start_date', $now, '<=');
        $query->condition($condition_start_date);

        $condition_end_date = $query->orConditionGroup();
        $condition_end_date->notExists('end_date');
        $condition_end_date->condition('end_date', $now, '>=');
        $query->condition($condition_end_date);

        $condition_roles = $query->orConditionGroup();
        $condition_roles->notExists('roles');
        foreach($roles as $role) {
            $condition_roles->condition('roles', $role, 'IN');
        }
        $query->condition($condition_roles);

        $condition_content_types = $query->orConditionGroup();
        $condition_content_types->notExists('content_types');
        if($nid) {
            $node = Node::load($nid);
            if($node) {
                $condition_content_types->condition('content_types', $node->bundle(), 'IN');
            }
        }
        $query->condition($condition_content_types);

        $notificationIds = $query->execute();
        $notifications = \Drupal::entityTypeManager()->getStorage("notification")->loadMultiple($notificationIds);

        $filteredNotifications = $this->applyFilter($notifications, $path);

        return $filteredNotifications;
    }


    public function applyFilter(array $notifications, string $path): array {
        $filteredNotifications = [];
        if(!empty($notifications)) {
            foreach($notifications as $notification) {
                $restrictedPages = $notification->get("pages")->value;
                if($restrictedPages) {
                    $restrictedPages = preg_split('/\r\n|\r|\n/', $restrictedPages);
                }
                if(empty($restrictedPages) || in_array($path, $restrictedPages)) {
                    $filteredNotifications[] = $notification;
                }
            }
        }
        return $filteredNotifications;
    }

}