<?php

namespace Drupal\notification_popin\Controller;

use \Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Drupal\notification_popin\NotificationManager;
use \Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller routines for notification routes.
 */
class NotificationController extends ControllerBase {

  /**
   * The notification manager.
   *
   * @var \Drupal\notification_popin\NotificationManager
   */
  protected $notificationManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a NotificationController object.
   *
   * @param \Drupal\notification_popin\NotificationManager $notificationManager
   * @param \Drupal\Core\Render\RendererInterface $renderer
   */
  public function __construct(NotificationManager $notificationManager, RendererInterface $renderer) {
    $this->notificationManager = $notificationManager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('notification_manager'),
      $container->get('renderer')
    );
  }

  /**
   * Returns all notifications.
   *
   */
  public function getNotifications() {
    $query = \Drupal::request()->query;
    $path = $query->get('path');
    $nid = $query->get('nid');
    $response = [];
    $notifications = $this->notificationManager->getNotifications($path, $nid);
    if(!empty($notifications)) {
      $view_builder = \Drupal::entityTypeManager()->getViewBuilder('notification');
      foreach($notifications as $notification) {
        $renderArray = $view_builder->view($notification, "full");
        $response[] = [
            "uuid" => $notification->uuid(),
            "title" => $notification->get("label")->value,
            "content" => $this->renderer->render($renderArray),
        ];
      }
    }
    return new JsonResponse($response);
  }


}
