<?php declare(strict_types = 1);

namespace Drupal\notification_popin;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a notification entity type.
 */
interface NotificationInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
