# Notification Popin

## About this module
This module allows you to create notification content which is displayed inside a popin on the site.

The notification content is a fieldable entity and can be customized as you need.
By default you can setup a title and a rich text content.

You can configure the visibility of notification with : 
  - start date
  - end date
  - user role
  - content type
  - page path

All are optionals.

The popin will be displayed once per user by using the js local storage.

## Installation

You can download this module with composer, and enable it like any other module.

After that, you can manage your notifications here : /admin/content/notification
