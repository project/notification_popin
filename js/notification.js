
const KEY = "Drupal.notification_popin.viewed";

let viewedNotifications = localStorage.getItem(KEY) ? localStorage.getItem(KEY).split(',') : [];

let url = drupalSettings.notification_popin.apiPath;
let nid = drupalSettings.path.currentPath.startsWith("node/") ? drupalSettings.path.currentPath.substring(5) : null;

 let params =  new URLSearchParams({
    path: window.location.pathname,
    nid: nid,
});

fetch(url + "?" + params)
    .then((response) => {
        return response.json();
    })
    .then((notifications) => {
        notifications.forEach(notification => {
            if(!viewedNotifications.includes(notification.uuid)) {
                showNotification(notification);
                viewedNotifications.push(notification.uuid);
                localStorage.setItem(KEY, viewedNotifications);
            }
            else {
                document.querySelector('.notification').remove();
            }
        });
        // add listener
        document.querySelectorAll('.notification .close').forEach(function(e) {
            e.addEventListener('click', function(e) {
                let uuid = e.target.closest(".notification-w").id;
                closeNotification(uuid);
            });
        });
    });
        

function showNotification(notification) {
    if(!document.querySelector('.notification-overlay')) {
        let overlay = document.createElement('div');
        overlay.classList.add('notification-overlay');
        document.body.appendChild(overlay);
        // add listener
        document.querySelector('.notification-overlay').addEventListener('click', function() {
            let notifications = document.querySelectorAll('.notification-w');
            let lastNotification = notifications[notifications.length - 1].id;
            closeNotification(lastNotification);
        });
    }

    let popin = document.createElement('div');
    popin.classList.add('notification-w');
    popin.setAttribute('id', notification.uuid);
    popin.innerHTML = notification.content;
    document.body.appendChild(popin);
}


function closeNotification(uuid) {
    document.getElementById(uuid).remove();
    if(!document.querySelector('.notification')) {
        document.querySelector('.notification-overlay').remove();
    }
}